const { default: mongoose } = require('mongoose')
const Room = require('./models/Room')
const Building = require('./models/Building')
mongoose.connect('mongodb://localhost:27017/example')

async function main () {
  const newinformaticsBuilding = await Building.findById('6218e6e9cfcfef967c887088')
  const room = await Room.findById('6218e6e9cfcfef967c88708b')
  const informaticsBuilding = await Building.findById(room.building)
  console.log(newinformaticsBuilding)
  console.log(room)
  console.log(informaticsBuilding)
  room.building = newinformaticsBuilding
  newinformaticsBuilding.rooms.push(room)
  informaticsBuilding.rooms.pull(room)
  room.save()
  newinformaticsBuilding.save()
  informaticsBuilding.save()
}

main().then(() => {
  console.log('Finish')
})
